from setuptools import setup, find_packages

setup(
    name="yosys_kernel",
    version="0.1",
    packages=find_packages(),
    install_requires=['metakernel'],
)
