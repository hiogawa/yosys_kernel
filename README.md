Yosys shell for IPython Kernel

# Installation

```
$ pip install .
$ python -m yosys_kernel install # --help option will show some choices about install location
```

# References

- [Yosys](https://github.com/YosysHQ/yosys)
- [MetaKernel](https://github.com/Calysto/metakernel)
