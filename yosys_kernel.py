from metakernel import REPLWrapper, ProcessMetaKernel
import sys

class YosysKernel(ProcessMetaKernel):
    implementation = 'Yosys'
    implementation_version = '0.1'
    language = 'no-op'
    language_version = '0.1'
    language_info = {
        'name': 'text',
        'mimetype': 'text/plain',
        'file_extension': '.ys',
    }
    banner = "Yosys Kernel"
    kernel_json = {
        'argv': [ sys.executable, '-m', 'yosys_kernel', '-f', '{connection_file}'],
        'display_name': 'Yosys',
        'language': 'no-op',
        'name': 'yosys_kernel'
    }

    def makeWrapper(self):
      return REPLWrapper('yosys', 'yosys> ', None)

if __name__ == '__main__':
    YosysKernel.run_as_main()
